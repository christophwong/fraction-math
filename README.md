# Fractions Calculator in Ruby

Ruby command line program that handles math operations on fractions

## Up and Running

After cloning this repository, we can start this program by running `rake start` (or `rake s` for shorthand) to start the program.
Then, try entering fraction operations, such as:

`1/2 * 5_3/4`

You should expect to see `2_7/8` in the output.

## Tests

To run test, use `rake test`

### Note:

As shown in the example, mixed numbers are represented by whole_numerator/denominator. e.g. "3_1/4"
