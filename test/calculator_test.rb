require 'minitest/autorun'
require_relative '../lib/calculator.rb'

class TestCalculator < Minitest::Test
  def test_calculator_does_fraction_math
    args = [(1/2r), "+", (1/4r)]
    assert_equal (3/4r), Calculator.calculate(*args)
  end
end
