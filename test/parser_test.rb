require 'minitest/autorun'
require_relative '../lib/parser.rb'

class TestParser < Minitest::Test
  def test_parser_returns_parsed_values
    assert_equal 0, Parser.parseFrationsString('0')
    assert_equal 13/4r, Parser.parseFrationsString('3_1/4')
    assert_equal 1/4r, Parser.parseFrationsString('1/4')
    assert_equal 1, Parser.parseFrationsString('1')
  end

  def test_parser_with_whole_input
    result = Parser.parseWholeInput('1/2 * 3_1/4')
    assert_equal [(1/2r), "*", (13/4r)], result
  end

  def test_parser_can_turn_improper_fractions_back_to_mixed_numbers
    assert_equal "0",  Parser.improperFractionsToMixedNumbers(0)
    assert_equal "1_2/5", Parser.improperFractionsToMixedNumbers(7/5r)
    assert_equal "2_4/5", Parser.improperFractionsToMixedNumbers(14/5r)
  end
end
