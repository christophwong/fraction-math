require 'minitest/autorun'
require_relative '../lib/runner.rb'

class TestRunner < Minitest::Test
  def test_runner_passes_user_input_to_calculator_when_it_runs
    input_string = "test string"
    parsed_values = ["some", "*", "operations"]
    result = 11/3
    unparsed_result = "3_2/3"

    uiMethodMock = MiniTest::Mock.new
    uiMethodMock.expect(:call, input_string, [$stdin])
    uiMethodMock.expect(:call, nil, [unparsed_result, $stdout])

    parserMethodMock = MiniTest::Mock.new
    parserMethodMock.expect(:call, parsed_values, [input_string])
    parserMethodMock.expect(:call, unparsed_result, [result])

    calcMethodMock = MiniTest::Mock.new
    calcMethodMock.expect(:call, result, [*parsed_values])

    UI.stub(:receive, uiMethodMock) do
      Parser.stub(:parseWholeInput, parserMethodMock) do
        Calculator.stub(:calculate, calcMethodMock) do
          Parser.stub(:improperFractionsToMixedNumbers, parserMethodMock) do
            UI.stub(:give, uiMethodMock) do
              Runner.run
            end
          end
        end
      end
    end

    uiMethodMock.verify
    parserMethodMock.verify
    calcMethodMock.verify
  end
end
