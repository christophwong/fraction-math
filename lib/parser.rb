require_relative './operators.rb'

class Parser
  class << self
    def parseFrationsString(str)
      arr = str.split('_')
      arr.reduce (0) { |sum, n|
        sum + n.to_r
      }
    end

    def improperFractionsToMixedNumbers(improper_fraction)
      whole_number = improper_fraction.to_i
      proper_fraction = improper_fraction - whole_number

      if proper_fraction == 0
        whole_number.to_s
      else
        "#{whole_number.to_s}_#{proper_fraction.to_s}"
      end
    end

    def parseWholeInput(str)
      str.split(' ').map { |inputValue|
        if isOperator?(inputValue)
          inputValue
        else
          parseFrationsString inputValue
        end
      }
    end

    private

    def isOperator?(value)
      OPERATORS.member? value.to_sym
    end
  end
end
