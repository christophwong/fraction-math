require_relative './ui.rb'
require_relative './parser.rb'
require_relative './calculator.rb'

class Runner
  def initialize
  end

  def self.run
    UI.give('Welcome, please enter your fraction operation:', $stdout)

    input = UI.receive($stdin)
    parsed_input = Parser.parseWholeInput(input)
    result = Calculator.calculate(*parsed_input)
    formatted_result = Parser.improperFractionsToMixedNumbers(result)
    UI.give(formatted_result, $stdout)
  end
end
