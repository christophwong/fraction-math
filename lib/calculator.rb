class Calculator
  class << self
    def calculate(*args)
      op1, operator, op2 = args
      op1.send(operator, op2)
    end
  end
end
