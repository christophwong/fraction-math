class UI
  class << self
    def give(message, io)
      io.puts(message)
    end

    def receive(io)
      io.gets.chomp
    end
  end
end
